Introduction:
-------------------
This module provide functionality to animating a simple news ticker.

Requirements:
------------------
This module requires the following modules:
 * None

Installation:
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.

How To Use:
-----------------
* Download library from following link
 https://benjaminrh.github.io/jquery-ticker/
* Extract in "libraries/jquery_view_ticker" folder.
* Go to "admin/structure/views" and create view.
* Select "Format" as "jQuery View Ticker"
* Configure animation from setting.
* Select title of the node from "Fields" section.
* Save view.
